from redis import StrictRedis


def get_redis_client():
    return StrictRedis(host="localhost", port=6379, db=0, decode_responses=True)
