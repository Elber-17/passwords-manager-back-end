from utils.redis_util import get_redis_client


def check_if_token_is_revoked(decrypted_token):
    client = get_redis_client()
    jti = decrypted_token["jti"]
    entry = client.get(jti)

    if entry is None:
        return False

    return entry == "true"
