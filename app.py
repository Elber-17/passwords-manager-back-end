from flask import Flask
from flask import request
from flask_cors import CORS
from flask_jwt_extended import JWTManager

from flask_migrate import Migrate

from core.sqlalchemy import db
from config.db import URI
from config.token_life import ACCESS_TOKEN_LIFE
from errors.token_errors import TokenErrorHandler

from utils.token_black_list_util import check_if_token_is_revoked

from validations.user import validate_user_requests
from validations.login import validate_login_requests

from resources.user import User
from resources.session import Session

app = Flask(__name__)

app.config["SQLALCHEMY_DATABASE_URI"] = URI
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["JWT_SECRET_KEY"] = "super-secret"
app.config["JWT_ACCESS_TOKEN_EXPIRES"] = ACCESS_TOKEN_LIFE
app.config["JWT_TOKEN_LOCATION"] = ["headers"]
app.config["JWT_BLACKLIST_ENABLED"] = True

db.init_app(app)
migrate = Migrate(app, db)
CORS(app, supports_credentials=True)
jwt = JWTManager(app)

jwt.token_in_blacklist_loader(check_if_token_is_revoked)
jwt.revoked_token_loader(TokenErrorHandler.revoken_token)
jwt.unauthorized_loader(TokenErrorHandler.unauthorized_token)
jwt.expired_token_loader(TokenErrorHandler.expired_token)


@app.route("/user", methods=["GET", "POST", "PATCH", "DELETE"])
@validate_user_requests
def user(body):
    user_resource = User()
    method = getattr(user_resource, request.method.lower())

    return method(body)


@app.route("/session/login", methods=["POST"])
@validate_login_requests
def login(body):
    session = Session()

    return session.login(body)


@app.route("/session/check", methods=["GET"])
def check_session():
    session = Session()

    return session.check_session()


@app.route("/session/logout", methods=["DELETE"])
def logout():
    session = Session()

    return session.logout()


# @app.route('/account', methods=['GET'])
# @jwt_required
# def account():
#     _account_ = Account()

#     return _account_.get()


# @app.route('/user/accounts', methods=['GET', 'POST', 'PATCH', 'DELETE'])
# @jwt_required
# def user_accounts():
#     _user_accounts_ = UserAccounts()

#     if request.method == 'GET':
#         return _user_accounts_.get()

#     elif request.method == 'POST':
#         return _user_accounts_.post()

#     elif request.method == 'PATCH':
#         return _user_accounts_.patch()

#     elif request.method == 'DELETE':
#         return _user_accounts_.delete()


if __name__ == "__main__":
    app.run(use_debugger=True, host="0.0.0.0", port=8888, use_reloader=True)
