"""tablas_principales

Revision ID: 5825e7d27568
Revises: 
Create Date: 2020-12-26 16:38:50.355758

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import BIGINT
from sqlalchemy.dialects.postgresql import VARCHAR
from sqlalchemy.dialects.postgresql import DATE


# revision identifiers, used by Alembic.
revision = '5825e7d27568'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
	op.create_table(
		'user',
		sa.Column('id', BIGINT, primary_key=True, autoincrement=True),
		sa.Column('email', VARCHAR(128), nullable=False, unique=True),
		sa.Column('username', VARCHAR(128)),
		sa.Column('password', VARCHAR(1024), nullable=False),
		sa.Column('delete_at', DATE),
	)
	
	op.create_table(
		'account',
		sa.Column('id', BIGINT, primary_key=True, autoincrement=True),
		sa.Column('name', VARCHAR(128), nullable=False),
		sa.Column('description', VARCHAR(264)),
		sa.Column('path_img', VARCHAR(5120)),
		sa.Column('delete_at', DATE),
	)

	op.create_table(
		'user_accounts',
		sa.Column('id', BIGINT, primary_key=True, autoincrement=True),
		sa.Column('user_id', BIGINT, sa.ForeignKey('user.id'), nullable=False),
		sa.Column('account_id', BIGINT, sa.ForeignKey('account.id')),
		sa.Column('email', VARCHAR(128), nullable=False),
		sa.Column('username', VARCHAR(128)),
		sa.Column('password', VARCHAR(1024), nullable=False),
		sa.Column('delete_at', DATE),
	)


def downgrade():
	pass
