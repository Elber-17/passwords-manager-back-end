from sqlalchemy.exc import IntegrityError

from core.resource import BaseResource

class SQLAlchemyErrorsHandler(BaseResource):
    def response(self, error):
        if (type(error) == IntegrityError):
            return self.integrity_error_handler(error)

        return self.make_response({'message': 'Error inesperadisimo'}, 500)

    def integrity_error_handler(self, error):
        return self.make_response({'message': error.orig.args[0].split(': ')[1].replace('(', '').replace(')', '').strip()}, 400)    