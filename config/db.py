params = {
		'dialect' : 'postgresql',
		'driver' : 'psycopg2',
		'username' : 'posglobal',
		'password' : '',
		'ip' : 'localhost',
		'port' : 5432,
		'database' : 'password-manager',
		}

URI = '{dialect}+{d}://{u}:{p}@{ip}:{port}/{db}'.format(dialect=params['dialect'], 
															   								d=params['driver'],
																							u=params['username'],
													 		   								p=params['password'],
																							ip=params['ip'],
																							port=params['port'],
																							db=params['database'])