from functools import wraps
from flask import request
from flask.wrappers import Response

from core.resource import BaseResource

baseResource = BaseResource()


def validate_post():
    body = request.get_json()

    if not body:
        return baseResource.make_response(
            {"Bad Request": "No has mandado los campos para registrar usuario"}
        )

    required_fields = ["password"]
    optional_fields = ["username", "email"]

    for key in body.keys():
        if key not in required_fields and key not in optional_fields:
            return baseResource.make_response(
                {"Bad Request": f"El parametro '{key}' no es un campo registrable."},
                400,
            )

        if key in required_fields:
            required_fields.remove(key)

        elif key in optional_fields:
            optional_fields.remove(key)

    if len(optional_fields) > 1:
        return baseResource.make_response(
            {"Bad Request": "Debes mandar el 'username' o el 'email'"}, 400
        )

    elif len(optional_fields) < 1:
        return baseResource.make_response(
            {
                "Bad Request": "Solo puedes logearte con el 'username' o el 'email', no con ambos"
            },
            400,
        )

    if len(required_fields) > 0:
        fields_not_received = ", ".join(required_fields)
        return baseResource.make_response(
            {
                "Bad Request": f"Los siguientes parametros son obligatorios: '{fields_not_received}'"
            },
            400,
        )

    for key, value in body.items():
        if not value:
            return baseResource.make_response(
                {"Bad Request": f"El parametro '{key}' no puede estar vacío."}, 400
            )

    return body


def validate_login_requests(function):
    @wraps(function)
    def decorated_function():
        body = {}

        if request.method == "GET":
            pass

        if request.method == "POST":
            body = validate_post()

        if request.method == "PATCH":
            pass

        if request.method == "DELETE":
            pass

        if isinstance(body, Response):
            return body

        return function(body)

    return decorated_function
