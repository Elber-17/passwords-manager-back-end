from core.resource import BaseResource

from models.account import Account as ModelAccount

class Account(BaseResource):
    def get(self):
        accounts = ModelAccount.query.all()
        accounts_response = []

        for account in accounts:
            account_response = {}
            account_response['id'] = account._id_
            account_response['name'] = account.name
            account_response['url_img'] = account.url_img

            accounts_response.append(account_response)

        return self.make_response(accounts_response, 200)