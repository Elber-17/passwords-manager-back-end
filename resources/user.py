from flask import request
from flask_jwt_extended import jwt_required
from flask_jwt_extended import get_jwt_identity
from bcrypt import hashpw
from bcrypt import gensalt

from core.resource import BaseResource
from core.sqlalchemy import db
from db_models.user import User as ModelUser
from errors.sqlalchemy_errors import SQLAlchemyErrorsHandler


class User(BaseResource):
    @jwt_required
    def get(self):
        user_id = get_jwt_identity()
        user = ModelUser.query.get(user_id)
        response = {
            "username": user.username,
            "password": user.password,
            "email": user.email,
            "creationDate": str(user.creation_date.date()),
        }

        return self.make_response(response, 200)

    def post(self, body):
        username = body.get("username", None)
        password = hashpw(body.get("password").encode("utf-8"), gensalt(12)).decode(
            "utf-8"
        )
        print(password)
        email = body.get("email")
        user = ModelUser(username=username, password=password, email=email)

        db.session.add(user)

        try:
            db.session.commit()
            response = {"message": "Usuario Registrado"}

            return self.make_response(response, 201)

        except Exception as error:
            handler = SQLAlchemyErrorsHandler()

            return handler.response(error)

    @jwt_required
    def patch(self):
        user_id = get_jwt_identity()

        username = request.args.get("username", False)
        password = request.args.get("password", False)
        email = request.args.get("email", False)

        if not username and not password and not email:
            return self.make_response(
                {"Bad Request": "No has enviado parametros para actualizar"}, 400
            )

        user = ModelUser.query.get(user_id)

        if username:
            user.username = username

        if email:
            user.email = email

        if password:
            user.password = password

        db.session.add(user)
        db.session.commit()

        return self.make_response({}, 204)

    @jwt_required
    def delete(self):
        user_id = get_jwt_identity()
        user = ModelUser.query.get(user_id)
        db.session.delete(user)
        db.session.commit()

        return self.make_response({}, 204)
