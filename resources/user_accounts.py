from flask import request
from flask_jwt_extended import get_jwt_identity
from sqlalchemy import desc

from core.resource import BaseResource
from core.sqlalchemy import db
from models.user_accounts import UserAccounts as ModelUserAccounts

class UserAccounts(BaseResource):
    def get(self):
        user_id = get_jwt_identity()
        user_accounts = ModelUserAccounts.query.filter_by(user_id=user_id).order_by(desc('id'))
        user_accounts_response = []

        for user_account in user_accounts:
            user_account_response = {}
            user_account_response['id'] = user_account._id_
            user_account_response['account'] = {
                'id' : user_account.account._id_,
                'name' : user_account.account.name,
                'dominantColor' : user_account.account.dominant_color,
                'urlImg' : user_account.account.url_img
            }
            user_account_response['username'] = user_account.username
            user_account_response['password'] = user_account.password

            user_accounts_response.append(user_account_response)

        return self.make_response(user_accounts_response, 200)
    
    def post(self):
        if not request.args.get('username', False):
            return self.make_response({'BAD REQUEST' : "Falta el parametro 'username'"}, 400)

        if not request.args.get('password', False):
            return self.make_response({'BAD REQUEST' : "Falta el parametro 'password'"}, 400)
        
        if not request.args.get('account_id', False):
            return self.make_response({'BAD REQUEST' : "Falta el parametro 'account_id'"}, 400)

        user_id = get_jwt_identity()
        username = request.args.get('username')
        password = request.args.get('password')
        account_id = request.args.get('account_id')

        user_account = ModelUserAccounts(
            user_id=user_id,
            account_id=account_id,
            username=username,
            password=password
        )

        db.session.add(user_account)
        db.session.commit()

        return self.make_response({'status' : 'ok'}, 201)
    
    def patch(self):
        if not request.args.get('id', False):
            return self.make_response({'BAD REQUEST' : "Falta el parametro 'id'"}, 400)

        if not request.args.get('username', False) and not request.args.get('password', False):
            return self.make_response({'BAD REQUEST' : "No hay parametros para actualizar"}, 400)

        user_id = get_jwt_identity()
        username = request.args.get('username', None)
        password = request.args.get('password', None)
        _id_ = request.args.get('id')
        user_account = ModelUserAccounts.query.filter_by(_id_=_id_).first()

        if username:
            user_account.username = username

        if password:
            user_account.password = password

        db.session.add(user_account)
        db.session.commit()

        return self.make_response({}, 204)
    
    def delete(self):
        if not request.args.get('id', False):
            return self.make_response({'BAD REQUEST' : "Falta el parametro 'id'"})

        user_id = get_jwt_identity()
        _id_ = request.args.get('id')
        user_account = ModelUserAccounts.query.filter_by(_id_=_id_).first()
        db.session.delete(user_account)
        db.session.commit()

        return self.make_response({}, 204)
