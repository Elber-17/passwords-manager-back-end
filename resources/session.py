from datetime import datetime
from time import time

from flask_jwt_extended import create_access_token
from flask_jwt_extended import jwt_required
from flask_jwt_extended import get_raw_jwt
from bcrypt import checkpw
from sqlalchemy import or_

from core.resource import BaseResource
from db_models.user import User
from utils.redis_util import get_redis_client


class Session(BaseResource):
    def login(self, body):
        username_or_email = body.get("username", body.get("email"))
        password = body["password"]
        user = self._get_user_(username_or_email, password)

        if not user:
            return self.make_response(
                {"Not Found": "El nombre de usuario o contraseña es incorrecto"}, 404
            )

        access_token = create_access_token(identity=username_or_email)

        response = self.make_response({"access_token": access_token})

        return self.make_response(response, 200)

    def _get_user_(self, username_or_email, password):
        user = User.query.filter(
            or_(User.username == username_or_email, User.email == username_or_email)
        ).first()

        if not user or not self._check_password_(user, password):
            return False

        return user

    def _check_password_(self, user, password):
        if checkpw(password.encode("utf-8"), user.password.encode("utf-8")):
            return True

        return False

    @jwt_required
    def check_session(self):
        return self.make_response({"message": "session valida"}, 200)

    @jwt_required
    def logout(self):
        self._add_token_to_black_list_()

        return self.make_response({"message": "session cerrada"})

    def _add_token_to_black_list_(self):
        raw_jwt = get_raw_jwt()
        jti = raw_jwt["jti"]
        exp = raw_jwt["exp"]
        exp_date = datetime.fromtimestamp(exp)
        print(exp_date)
        actual_date = datetime.fromtimestamp(time())
        print(actual_date)

        difference_date = exp_date - actual_date
        print(difference_date.seconds)

        redis_client = get_redis_client()
        redis_client.set(jti, "true", difference_date.seconds)
