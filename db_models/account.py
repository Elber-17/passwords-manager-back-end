from sqlalchemy import Column
from sqlalchemy.dialects.postgresql import BIGINT
from sqlalchemy.dialects.postgresql import VARCHAR
from sqlalchemy.dialects.postgresql import DATE

from core.sqlalchemy import db

class Account(db.Model):
    _id_ = Column('id', BIGINT(), primary_key=True)
    name = Column(VARCHAR(128), nullable=False)
    description = Column(VARCHAR(264))
    path_img = Column(VARCHAR(5120))
    delete_at = Column(DATE())
 