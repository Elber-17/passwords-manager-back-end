from sqlalchemy import Column
from sqlalchemy.dialects.postgresql import BIGINT
from sqlalchemy.dialects.postgresql import VARCHAR
from sqlalchemy.dialects.postgresql import DATE

from core.sqlalchemy import db

class User(db.Model):
    _id_ = Column('id', BIGINT(), primary_key=True)
    email = Column(VARCHAR(128), nullable=False)
    username = Column(VARCHAR(128))
    password = Column(VARCHAR(1024))
    delete_at = Column(DATE())
 