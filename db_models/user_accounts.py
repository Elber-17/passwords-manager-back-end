from sqlalchemy import Column
from sqlalchemy.dialects.postgresql import BIGINT
from sqlalchemy.dialects.postgresql import VARCHAR
from sqlalchemy.dialects.postgresql import DATE

from models.user import User
from models.account import Account

from core.sqlalchemy import db

class UserAccounts(db.Model):
    _id_ = Column('id', BIGINT(), primary_key=True)
    user_id = Column(BIGINT(), db.ForeignKey('user.id'))
    account_id = Column(BIGINT(), db.ForeignKey('account.id'))
    email = Column(VARCHAR(128), nullable=False)
    username = Column(VARCHAR(128))
    password = Column(VARCHAR(1024))
    delete_at = Column(DATE())

    user = db.relationship(User)
    account = db.relationship(Account)
 